﻿using System;
using ObjectGenerator.Classes;

namespace ObjectGenerator
{
    public class Generator
    {
        public static Random rnd = new Random();
        private static int pumpCount = 0;
        private static int pipeCount = 0;
        private static int tapCount = 0;

        public static Obj GenPump()
        {
            pumpCount++;

            var pump = new Pump()
            {
                Model = "Pump " + pumpCount,
                Pressure = rnd.Next(1, 100),
                Mass = rnd.Next(1, 20),
                Installation = DateTime.Now.AddDays(rnd.Next(-100, 100))
            };

            return pump;
        }

        public static Obj GenPipe()
        {
            pipeCount++;

            var pipe = new Pipe()
            {
                Material = "Pipe " + pipeCount,
                Diameter = rnd.Next(1, 100),
                Length = rnd.Next(1, 100),
                Mass = rnd.Next(1, 20),
                Installation = DateTime.Now.AddDays(rnd.Next(-100, 100))
            };

            return pipe;
        }

        public static Obj GenTap()
        {
            tapCount++;

            var tap = new Tap()
            {
                Model = "Tap " + tapCount,
                Diameter = rnd.Next(1, 100),
                Mass = rnd.Next(1, 20),
                Installation = DateTime.Now.AddDays(rnd.Next(-100, 100))
            };

            return tap;
        }

        static Func<Obj>[] procedures = new Func<Obj>[] {GenPump, GenPipe, GenTap};

        public static Obj GenObject()
        {
            var rndIndex = rnd.Next(3);

            return procedures[rndIndex]();
        }
    }
}
