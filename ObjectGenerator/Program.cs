﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using WebApi.Models;
using ObjectGenerator.Classes;
using System.Data.SqlClient;

namespace ObjectGenerator
{
    class Program
    {
        static void GenObjects()
        {
            int? IdParent = null;
            int index = 0;
            List<Obj> queue = new List<Obj>();
            List<int> ids = new List<int>();

            while (queue.Count < 1000000)
            {
                var childCount = IdParent == null ? 3 : Generator.rnd.Next(1, 5);

                for (int i = 0; i < childCount && queue.Count < 1000000; ++i)
                {
                    var obj = Generator.GenObject();
                    queue.Add(obj);
                    var id = CatalogRepository.CreateObjectAsync(IdParent, obj.ToXML("Object " + queue.Count)).Result;
                    ids.Add(id);

                    Console.Write(" {0}", queue.Count);
                }

                IdParent = ids[index++];
            }
        }

        public void InsertFromFile()
        {
            string connectionString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;

            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                conn.Open();

                using (var file = new StreamReader(@"C:\tmp\dbo.Attribute.data.sql"))
                {
                    string line = "";
                    int cnt = 0;
                    while (line != null)
                    {
                        List<string> rows = new List<string>();

                        for (int i = 0; i < 100000; ++i, cnt++)
                        {
                            line = file.ReadLine();
                            if (line == null)
                                break;
                            else
                                rows.Add(line);
                        }

                        Console.WriteLine("read {0}", cnt);

                        var command = conn.CreateCommand();
                        command.CommandText = string.Join("\n", rows);
                        command.CommandTimeout = 100000;
                        command.ExecuteNonQuery();

                        Console.WriteLine("write ok");
                    }
                }
            }
        }

        static void Main(string[] args)
        {
            
        }
    }
}
