﻿using System;
using System.Reflection;
using System.Xml;

namespace ObjectGenerator.Classes
{
    public class Obj
    {
        public XmlDocument ToXML(string name)
        {
            var xDoc = new XmlDocument();

            var objElement = xDoc.CreateElement("object");

            var objNameAttr = xDoc.CreateAttribute("name");
            objNameAttr.AppendChild(xDoc.CreateTextNode(name));
            objElement.Attributes.Append(objNameAttr);

            var classnameAttr = xDoc.CreateAttribute("classname");
            classnameAttr.AppendChild(xDoc.CreateTextNode(this.GetType().Name));
            objElement.Attributes.Append(classnameAttr);

            xDoc.AppendChild(objElement);

            PropertyInfo[] properties = this.GetType().GetProperties(BindingFlags.Public | BindingFlags.Instance);

            foreach (var p in properties)
            {
                if(p.Name == "XML")
                {
                    continue;
                }

                var elem = xDoc.CreateElement("Property");

                var nameAttr = xDoc.CreateAttribute("name");
                nameAttr.AppendChild(xDoc.CreateTextNode(p.Name));
                elem.Attributes.Append(nameAttr);

                var valueAttr = xDoc.CreateAttribute("value");
                
                if (p.PropertyType == typeof(DateTime))
                {
                    valueAttr.AppendChild(xDoc.CreateTextNode(((DateTime)p.GetValue(this)).ToString("s")));
                }
                else
                {
                    valueAttr.AppendChild(xDoc.CreateTextNode(p.GetValue(this).ToString()));
                }
                elem.Attributes.Append(valueAttr);

                objElement.AppendChild(elem);
            }

            return xDoc;
        }
    }
}
