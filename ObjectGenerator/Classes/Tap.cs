﻿using System;

namespace ObjectGenerator.Classes
{
    public class Tap : Obj
    {
        public string Model { get; set; }
        public float Diameter { get; set; }
        public float Mass { get; set; }
        public DateTime Installation { get; set; }
    }
}