﻿using System;

namespace ObjectGenerator.Classes
{
    public class Pump : Obj
    {
        public string Model { get; set; }
        public float Pressure { get; set; }
        public float Mass { get; set; }
        public DateTime Installation { get; set; }
                
    }
}