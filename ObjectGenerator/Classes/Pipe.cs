﻿using System;

namespace ObjectGenerator.Classes
{
    public class Pipe : Obj
    {
        public string Material { get; set; }
        public float Diameter { get; set; }
        public float Length { get; set; }
        public float Mass { get; set; }
        public DateTime Installation { get; set; }
    }
}