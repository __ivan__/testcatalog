﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using WebApi.Models;
using System.Xml;
using System.Text;
using Newtonsoft.Json.Linq;
using System.Threading.Tasks;
using NLog;
using Newtonsoft.Json;

namespace WebApi.Controllers
{
    [RoutePrefix("api/catalog")]
    public class CatalogController : ApiController
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();

        /// <summary>
        /// Добавление нового объекта.
        /// </summary>
        /// <param name="data">Данные объекта.</param>
        /// <param name="IdParent">Id родителя. null - если будет корнем нового дерева.</param>
        /// <returns>Id созданного объекта.</returns>
        [HttpPost]
        [Route("{IdParent:int?}")]
        public async Task<int> CreateObject([FromBody]JObject data, int? IdParent = null)
        {
            logger.Info("CreateObject: IdParent = {0}", IdParent);

            try
            {
                var xml = CatalogRepository.JsonToXML(data);
                return await CatalogRepository.CreateObjectAsync(IdParent, xml);
            }
            catch
            {
                logger.Error("CreateObject: IdParent = {0} NOT FOUND", IdParent);

                throw new HttpResponseException(HttpStatusCode.NotFound);
            }
        }

        /// <summary>
        /// Получение объекта
        /// </summary>
        /// <param name="idCatalog">Id объекта в таблице Catalog</param>
        /// <returns>Объект в формате JSON.</returns>
        [HttpGet]
        [Route("{idCatalog:int}")]
        public async Task<HttpResponseMessage> GetObject(int idCatalog)
        {
            logger.Info("GetObject: idCatalog = {0}", idCatalog);

            try
            {
                var xml = await CatalogRepository.GetObjectAsync(idCatalog);

                var json = JsonConvert.SerializeXmlNode(xml, Newtonsoft.Json.Formatting.Indented);

                var result = new HttpResponseMessage()
                {
                    Content = new StringContent(json.Replace("\"@", "\""), Encoding.UTF8, "application/json")
                };

                return result;
            }
            catch
            {
                logger.Error("GetObject: idCatalog = {0} NOT FOUND", idCatalog);

                throw new HttpResponseException(HttpStatusCode.NotFound);
            }
        }

        /// <summary>
        /// Получить поддерево объектов
        /// </summary>
        /// <param name="idRoot">Id корневого объекта</param>
        /// <returns>Список из Id, названия и Id родителя.</returns>
        [HttpGet]
        [Route("{idRoot:int}/subtree")]
        public async Task<IEnumerable<WebApi.Models.Catalog>> GetSubtree(int idRoot)
        {
            logger.Info("GetSubtree: idRoot = {0}", idRoot);

            try
            {
                return await CatalogRepository.GetSubtreeAsync(idRoot);
            }
            catch
            {
                logger.Error("GetSubtree: idRoot = {0} NOT FOUND", idRoot);

                throw new HttpResponseException(HttpStatusCode.NotFound);
            }
        }

        /// <summary>
        /// Получить отфильтрованные данные по условию из задания.
        /// </summary>
        /// <param name="idCatalog">Id корневого объекта.</param>
        /// <returns>Список из Id и названий.</returns>
        [HttpGet]
        [Route("{idCatalog:int}/filtered")]
        public async Task<IEnumerable<Catalog>> GetFiltered(int idCatalog)
        {
            logger.Info("GetFiltered: idCatalog = {0}", idCatalog);

            try
            {
                return await CatalogRepository.GetFilteredAsync(idCatalog);
            }
            catch
            {
                logger.Info("GetFiltered: idCatalog = {0} NOT FOUND", idCatalog);

                throw new HttpResponseException(HttpStatusCode.NotFound);
            }
        }
    }
}
