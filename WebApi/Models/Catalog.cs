﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Microsoft.SqlServer.Types;

namespace WebApi.Models
{
    public class Catalog
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int IdParent { get; set; }
    }
}