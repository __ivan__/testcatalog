﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using Dapper;
using System.Xml;
using Newtonsoft.Json.Linq;
using System.Threading.Tasks;

namespace WebApi.Models
{
    /// <summary>
    /// Работа с базой + конвертация JSON в XML.
    /// </summary>
    public class CatalogRepository
    {
        static string connectionString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
        
        public static async Task<int> CreateObjectAsync(int? IdParent, XmlDocument xml)
        {
            using (var conn = new SqlConnection(connectionString))
            {
                return await conn.ExecuteScalarAsync<int>(
                    "dbo.CreateObject", 
                    new {
                        IdParent = IdParent,
                        xml = xml
                    }, 
                    commandType: System.Data.CommandType.StoredProcedure
                );
            }
        }

        public static async Task<XmlDocument> GetObjectAsync(int idCatalog)
        {
            using (var conn = new SqlConnection(connectionString))
            {
                return await conn.ExecuteScalarAsync<XmlDocument>(
                    "dbo.GetObject",
                    new { IdCatalog = idCatalog },
                    commandType: System.Data.CommandType.StoredProcedure
                );
            }
        }

        public static async Task<IEnumerable<Catalog>> GetSubtreeAsync(int idRoot)
        {
            using (var conn = new SqlConnection(connectionString))
            {
                return await conn.QueryAsync<Catalog>(
                    "dbo.GetSubtree",
                    new { IdRoot = idRoot },
                    commandType: System.Data.CommandType.StoredProcedure
                );
            }
        }

        public static async Task<IEnumerable<Catalog>> GetFilteredAsync(int idCatalog)
        {
            using (var conn = new SqlConnection(connectionString))
            {
                return await conn.QueryAsync<Catalog>(
                    "dbo.GetFiltered",
                    new { IdCatalog = idCatalog },
                    commandType: System.Data.CommandType.StoredProcedure
                );
            }
        }

        public static XmlDocument JsonToXML(JObject json)
        {
            var xDoc = new XmlDocument();

            var objElement = xDoc.CreateElement("object");

            var objNameAttr = xDoc.CreateAttribute("name");
            var name = json.GetValue("name").ToString();
            objNameAttr.AppendChild(xDoc.CreateTextNode(name));
            objElement.Attributes.Append(objNameAttr);

            var classnameAttr = xDoc.CreateAttribute("classname");
            var classname = json.GetValue("classname").ToString();
            classnameAttr.AppendChild(xDoc.CreateTextNode(classname));
            objElement.Attributes.Append(classnameAttr);

            xDoc.AppendChild(objElement);

            foreach (var p in json)
            {
                if (p.Key == "name" || p.Key == "classname")
                {
                    continue;
                }

                var elem = xDoc.CreateElement("Property");

                var nameAttr = xDoc.CreateAttribute("name");
                nameAttr.AppendChild(xDoc.CreateTextNode(p.Key));
                elem.Attributes.Append(nameAttr);

                var valueAttr = xDoc.CreateAttribute("value");
                valueAttr.AppendChild(xDoc.CreateTextNode(p.Value.ToString()));
                
                elem.Attributes.Append(valueAttr);

                objElement.AppendChild(elem);
            }

            return xDoc;
        }
    }
}